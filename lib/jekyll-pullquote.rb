# frozen_string_literal: true

require 'jekyll/pullquote/version'
require 'jekyll/pullquote/utils'

module Jekyll
  class Pullquote < Liquid::Block
    def initialize(tag_name, markup, tokens)
      @markup = markup
      super
    end

    def render(context)
      output = super
      quotemarks = /\{"\s+(.+?)\s+"\}/m
      if output =~ quotemarks

        # Mute the pullquote selection tag in the original text
        output.gsub!(quotemarks, '\1')

        # Capture the pullquote selection
        quote = $1

        # Capture any desired classes
        classnames = @markup.sub(/pullquote/i, '').strip

        output = "<span class='pullquote #{classnames}' data-pullquote='#{quote}'></span>#{output}"

      else
        raise "Pullquote Error: Surround your pullquote like this {\" text to be quoted \"}"
      end
    end
  end
end

Liquid::Template.register_tag('pullquote', Jekyll::Pullquote)
