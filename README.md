# NO LONGER MAINTAINED

I'm no longer actively maintaining this. It does what it does well enough, but certainly check all issues (open and closed) to see known caveats.

# Jekyll PullQuotes

Jekyll PullQuotes creates a new Liquid tagging scheme to create semantically correct pullquotes. Credit where credit's due: Brandon Mathis wrote the [pullquote plugin](https://github.com/octopress/pullquote-tag) this was forked from. This version corrects incompatibilities with newer Jekyll versions, seeks more robust placement and usage options, and is actively maintained.

## Installation

### Using Bundler

Add this gem to your site's Gemfile in the `:jekyll_plugins` group:

    group :jekyll_plugins do
      gem 'jekyll-pullquote'
    end

Then install the gem with Bundler

    $ bundle

### Manual Installation

    $ gem install jekyll-pullquote

Then add the gem to your Jekyll configuration.

    gems:
      -jekyll-pullquote

## Usage

1. Surround the paragraph you want to quote from with a `{% pullquote %}` tag.
2. Surround the quote you want to use with `{"` and `"}`.

For example:

```
{% pullquote %}
When writing long-form posts, I find it helpful to include pull-quotes, which help
those scanning a post discern whether or not a post is helpful. It is important to 
note, {" pull-quotes are merely visual in presentation and should not appear twice in the text. "} That 
is why it is preferred to use a CSS only technique for styling pull-quotes.
{% endpullquote %}
```

This will output the following:

```html
<p><span data-pullquote="pullquotes are merely visual in presentation and should not appear twice in the text."></span>
When writing long-form posts, I find it helpful to include pull-quotes, which help
those scanning a post discern whether or not a post is helpful. It is important to 
note, pull-quotes are merely visual in presentation and should not appear twice in the text. That 
is why it is preferred to use a CSS only technique for styling pull-quotes.</p>
```

### Styling & Classnames

This plugin does not currently ship with its own CSS for styling. You may look to [Maykel Loomans](http://miekd.com/articles/pull-quotes-with-html5-and-css/) for inspiration.

Any text added to the pull-quote tag will be added as a classname too.

```
{% pullquote big %}              #=> class='pullquote big'
{% pullquote highlight %}   #=> class='pullquote highlight'
```
