# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jekyll/pullquote/version'

Gem::Specification.new do |spec|
  spec.name          = 'jekyll-pullquote'
  spec.version       = Pullquote::VERSION
  spec.authors       = ['Ron Scott-Adams']
  spec.email         = ['ron@tohuw.net']
  spec.summary       = 'Semantically correct pullquotes for Jekyll.'
  spec.homepage      = 'https://gitlab.com/tohuw/jekyll-pullquote'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'jekyll'

  spec.add_development_dependency 'bundler', '~> 1.6'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'clash'
end
